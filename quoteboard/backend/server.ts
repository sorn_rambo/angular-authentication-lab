import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import session from 'express-session';
import csurf from 'csurf';
import connectRedis from 'connect-redis';
import passport from 'passport';
import { UserController } from './controllers/user-controller';
import redis from 'redis';

const app = express();
const PORT = process.env.PORT || 3333;

// Create a Redis client
const redisClient = redis.createClient();

// Middleware and configurations
app.use(cors());
app.use(express.json());

// Session configuration
const RedisStore = connectRedis(session);

app.use(
  session({
    store: new RedisStore({ client: redisClient }),
    secret: '3#9K!pL@2oN5xG&8sD1zA$7yQ*4tH6w', // Replace with a strong secret key for session encryption
    resave: false,
    saveUninitialized: false,
    cookie: {
      secure: false, // Set to true in a production environment with HTTPS
      maxAge: 86400000, // Session duration in milliseconds (adjust as needed)
    },
  })
);

app.use(passport.initialize());
app.use(passport.session());
app.use(csurf());

// Connect to MongoDB
mongoose.connect('mongodb+srv://rambos:5LJObe4HUmGfLnXE@cluster0.vpn63iw.mongodb.net/authdb')
  .then(() => {
    console.log('MongoDB connected successfully');
  })
  .catch((err) => {
    console.error('MongoDB connection error:', err);
  });


// Routes
app.use('/api/users', UserController);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
