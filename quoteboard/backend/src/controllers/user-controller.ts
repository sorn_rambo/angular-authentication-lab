// src/controllers/user-controller.ts
import express from 'express';
import { UserService } from '../services/user.service';

const router = express.Router();
const userService = new UserService();

router.post('/register', userService.register);

export { router as UserController };
