// src/models/user.ts
import mongoose, { Document, Schema } from 'mongoose';

export interface UserDocument extends Document {
  username: string;
  password: string;
}

const userSchema = new Schema({
  username: String,
  password: String,
});

const User = mongoose.model<UserDocument>('User', userSchema);

export { User };
